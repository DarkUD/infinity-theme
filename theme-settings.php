<?php

/**
 * @file
 * Theme settings file for the infinity theme.
 */

require_once dirname(__FILE__) . '/template.php';

/**
 * Implements hook_form_FORM_alter().
 */
function infinity_form_system_theme_settings_alter(&$form, $form_state) {
  // You can use this hook to append your own theme settings to the theme
  // settings form for your subtheme. You should also take a look at the
  // 'extensions' concept in the Omega base theme.

// 	dpm($form);
// 	dpm($GLOBALS['theme_key']);
	
	// Some constants
	$theme 			= $GLOBALS['theme_key'];
	$path 			= drupal_get_path('theme', $theme);
	$settings 		= variable_get("theme_settings_".$theme, array());

	// We add the necessary Styles and Js files
	drupal_add_css($path . '/core/fonts/gb-font/gb-font.css' );
	drupal_add_css($path . '/core/fonts/caviar-dreams/caviar-dreams.css' );
	drupal_add_css($path . '/core/dashboard/css/infinity.dashboard.css' );
	drupal_add_css($path . '/core/third_party/plugins/utilities/colorpicker/css/colorpicker.css');

	drupal_add_js($path . '/core/third_party/plugins/utilities/trianglify/dist/trianglify.min.js');
	drupal_add_js($path . '/core/third_party/plugins/utilities/js-cookie/src/js.cookie.js');
	drupal_add_js($path . '/core/third_party/plugins/utilities/colorpicker/js/colorpicker.js');
	drupal_add_js($path . '/core/third_party/plugins/utilities/velocity/velocity.min.js');
	drupal_add_js($path . '/core/third_party/plugins/utilities/velocity/velocity.ui.min.js');
	drupal_add_js($path . '/core/third_party/plugins/utilities/tabbedcontent/jquery.tabbedcontent.min.js');

	drupal_add_js($path . '/core/third_party/helpers/ace/ace-builds/src-min-noconflict/ace.js');
	drupal_add_js($path . '/core/third_party/helpers/webfontloader/webfontloader.js');

	drupal_add_js($path . '/core/dashboard/js/dashboard.js');
	drupal_add_js($path . '/core/dashboard/js/dashboard.color.js');
	drupal_add_js($path . '/core/dashboard/js/dashboard.save.js');
	drupal_add_js($path . '/core/dashboard/js/dashboard.typography.js');
	drupal_add_js($path . '/core/dashboard/js/dashboard.profiles.js');
	
	// We gather the plugins

	$form["omega"]['infinity'] = array(
		"#type" => "fieldset",
		"#title" => "Infinity",
		"#weight" => -50,
	);

	$form["omega"]['infinity']['dashboard'] = array(
		"#type" => "container",
		"#attributes" => array(
				"class" => array("infinity-dashboard")
		)
	);

	$form["omega"]['infinity']['dashboard']['sidebar'] = array(
		"#markup" => infinity_sidebar(),
		"#prefix" => '<div class="sidebar-bg"></div><div class="sidebar">',
		"#suffix" => '</div> <!-- Sidebar -->'
	);

	$form["omega"]['infinity']['dashboard']['content'] = array(
		"#markup" => infinity_content(),
		"#prefix" => '<div class="content"> <div id="ajax-wrapper"></div>',
		"#suffix" => '</div> <!-- content -->'
	);

// 	$form["omega"]['infinity']['ajax_button'] = array(
// 		"#type" => "button",
// 		"#value" => "Ajax test",
// 		"#ajax" => array(
// 			"callback" => "infinity_ajax_callback",
// 			"wrapper" => "ajax-wrapper"
// 		)
// 	);
	
	// Now the structure to save the data
	
	$form["theme_key"] = array(
		"#type" => "value",
		"#value" => $theme
	);
	
	$form["omega"]['infinity']["data"]["grids"] = array(
		"#type" => "hidden",
		"#default_value" => isset($settings['grids']) ? json_encode($settings["grids"]) : '',
	);
	
	$form["omega"]['infinity']["data"]["colors"] = array(
		"#type" => "hidden",
		"#default_value" => isset($settings['colors']) ? json_encode($settings["colors"]) : '',
	);
	
	$form["omega"]['infinity']["data"]["custom_colors"] = array(
		"#type" => "hidden",
		"#default_value" => infinity_get_custom_file(DRUPAL_ROOT."/".$path."/less/colors/custom.colors.less"),
	);
	
	$form["omega"]['infinity']["data"]["fonts"] = array(
		"#type" => "hidden",
		"#default_value" => isset($settings['typography']) ? json_encode($settings["typography"]) : '',
	);
	
	$form["omega"]['infinity']["data"]["custom_typography"] = array(
		"#type" => "hidden",
		"#default_value" => infinity_get_custom_file(DRUPAL_ROOT."/".$path."/less/typography/custom.typography.less"),
	);
	
	$form["omega"]['infinity']["data"]["plugins"] = array(
		"#type" => "hidden",
		"#default_value" => isset($settings['plugins']) ? json_encode($settings["plugins"]) : '',
	);
	
	$form["omega"]['infinity']["data"]["icons"] = array(
		"#type" => "hidden",
		"#default_value" => isset($settings['icons']) ? json_encode($settings["icons"]) : '',
	);
	
	$form["omega"]['infinity']["data"]["custom_css"] = array(
		"#type" => "hidden",
		"#default_value" => infinity_get_custom_file(DRUPAL_ROOT."/".$path."/less/custom.less"),
	);
	
	$form["omega"]['infinity']["data"]["custom_js"] = array(
		"#type" => "hidden",
		"#default_value" => infinity_get_custom_file(DRUPAL_ROOT."/".$path."/js/custom.js"),
	);
	
	$form["omega"]['infinity']["data"]["save_profile"] = array(
		"#type" => "hidden",
		"#default_value" => false,
	);
	
	$form["omega"]['infinity']["data"]["delete_profile"] = array(
		"#type" => "hidden",
		"#default_value" => false,
	);
	
	$form["omega"]['infinity']["data"]["load_profile"] = array(
		"#type" => "hidden",
		"#default_value" => false,
	);
	
	$form["#submit"][] = "infinity_theme_settings_submit";
}

/**
 * Our custom submit function
 */
function infinity_theme_settings_submit($form,&$form_state){
	$values = $form_state["values"];
// 	dpm($values);
	
	$theme = $values["theme_key"];
	$path  = drupal_get_path('theme', $theme);
	
	$grids = json_decode($values["grids"]);
	$colors= json_decode($values["colors"]);
	$typography = json_decode($values["fonts"]);
	$plugins = json_decode($values["plugins"]);
	$icons = json_decode($values['icons']);
	
	$custom_colors = $values["custom_colors"];
	$custom_typography = $values["custom_typography"];
	$custom_css = $values["custom_css"];
	$custom_js = $values["custom_js"];
	
	
	// If we asked to load the profile
	if($values["load_profile"]){
		$loaded_data = infinity_load_profile($theme,$values["load_profile"]);
		//We replace the values with those of the loaded data
		
		dpm($loaded_data);
		
		$grids = $loaded_data['grids'];
		$colors = $loaded_data['colors'];
		$typography = $loaded_data['typography'];
		$plugins = $loaded_data['plugins'];
		$icons = $loaded_data['icons'];
		
		$custom_colors = $loaded_data["custom_color"];
		$custom_typography = $loaded_data["custom_typography"];
		$custom_css = $loaded_data["custom_css"];
		$custom_js = $loaded_data["custom_js"];
		
		drupal_set_message("Loaded Profile ".$loaded_data["name"]);		
	}
	
	$settings = variable_get("theme_settings_".$theme, array());
	
	$settings["grids"] = $grids;
	$settings["plugins"] = $plugins;
	$settings["icons"] = $icons;
	$settings["colors"] = $colors;
	$settings["typography"] = $typography;
	
	// We save the files now
	// The custom colors file ...
	infinity_create_colors_variables($colors);
	
	file_put_contents(DRUPAL_ROOT."/".$path."/less/colors/custom.colors.less", $custom_colors);

	// The typography
	infinity_create_fonts_variables($typography);
	infinity_create_js_loader($typography);
	
	file_put_contents(DRUPAL_ROOT."/".$path."/less/typography/custom.typography.less", $custom_typography);
	
	// The global custom less file ...
	file_put_contents(DRUPAL_ROOT."/".$path."/less/custom.less", $custom_css);
	
	// The global js file ...
	file_put_contents(DRUPAL_ROOT."/".$path."/js/custom.js", $custom_js);
	
	// And we compile the less
	$style = infinity_less_compile();
	file_put_contents(DRUPAL_ROOT."/".$path."/infinity_css/styles.css", $style);	
	
	// Finally we save the settings
	variable_set("theme_settings_".$theme, $settings);
	
	// We generate the files array that should be loaded on html preprocess
	infinity_prepare_files();
	
	// If we asked to save the profile
	if($values["save_profile"]){
		infinity_save_profile($theme,$values["save_profile"]);
	}
	
	// If we asked to delete the profile
	if($values["delete_profile"]){
		infinity_delete_profile($theme,$values["delete_profile"]);
	}	
}


/**
 * Return the content of the sidebar
 */
function infinity_sidebar(){

	$theme 	= $GLOBALS['theme_key'];
	$path 	= drupal_get_path('theme', $theme);

	$sections = infinity_dashboard_sections();

	$output = '<div class="header"><img src="/'. $path .'/core/dashboard/assets/logo.png"><h2>Infinity</h2></div>';
	$output .= '<ul id="sidebar-links">';

	$first = true;
	$class = "";

	foreach($sections as $section => $data){

		if($first){
			$class = " active";
			$first = false;
		}else{
			$class= "";
		}

		$output .= '<li class="' . $section . $class . '" data-target="' . $section . '">' .infinity_icon($data["icon"]) . ' '.t($data['title']).'</li>';
	}

	$output .= '</ul>';

	return $output;
}

/**
 * Return the content for the dashboard
 */
function infinity_content(){

	$base_url =  $GLOBALS["base_url"];
	$theme 	= $GLOBALS['theme_key'];
	$path 	= drupal_get_path('theme', $theme);
	$tpl_path = $path . "/core/dashboard/tpl/";

	$sections = infinity_dashboard_sections();

	$output = "";

	$first = true;
	
	$components = infinity_scan_components($theme);
	$profiles = infinity_scan_profiles($theme);

// 	dpm($components);
	
	foreach($sections as $section => $data){
		$vars = array(
			"section" => $section,
			"icon" => $data["icon"],
			"title" => $data['title'],
			"components" => $components,
			"profiles" => $profiles,
			"settings" => variable_get("theme_settings_".$theme, array())
		);

		if($first){
			$vars["classes"] = " active";
			$first = false;
		}
		
		$section_content = infinity_render_tpl($tpl_path . $section .".tpl.php",$vars);
		
		$output .= $section_content;
	}

	return $output;
}

/**
 * Return the html for a fonticon
 */
function infinity_icon($icon){
	return '<span class="infinity-icon icon-'.$icon.'"></span>';
}

/**
 * Return a list of components found
 */
function infinity_scan_components($theme,$categories=true){

	$components		= &drupal_static(__FUNCTION__,array());
	$path 			= drupal_get_path('theme', $theme);
	$components_dir = $path . "/" . "core/third_party";

	if (!function_exists('file_scan_directory')) {
		require_once DRUPAL_ROOT . '/includes/file.inc';
	}

	$masks = array(
		"frameworks" => '/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*\.framework$/',
		"plugins" 	=>	'/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*\.plugin$/'
	);
	
	foreach($masks as $type => $mask){
		$files = file_scan_directory($components_dir, $mask);
		
		foreach($files as $file){
			if (file_exists($file->uri)) {
				$component = infinity_parse_info($file->uri,$type,$categories);
				$components[$type] = $component;
			}
		}	
	}
	
	return $components;
}

/**
 * Return a list of profiles found
 */
function infinity_scan_profiles($theme,$categories=true){

	$profiles		= &drupal_static(__FUNCTION__,array());
	$path 			= drupal_get_path('theme', $theme);
	$profiles_dir = $path . "/" . "core/profiles";

	if (!function_exists('file_scan_directory')) {
		require_once DRUPAL_ROOT . '/includes/file.inc';
	}

	$masks = array(
		"profiles" => '/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*\.profile$/',
	);

	foreach($masks as $type => $mask){
		$files = file_scan_directory($profiles_dir, $mask);

		foreach($files as $file){
			if (file_exists($file->uri)) {
				$profile = infinity_parse_info($file->uri,$type,$categories);
				$profiles[$type] = $profile;
			}
		}
	}

	return $profiles;
}
/**
 * Parse an infinity setting file
 */
function infinity_parse_info($file,$type='',$categories=true){
	$info = &drupal_static(__FUNCTION__.$type."$categories",array());

	if (!file_exists($file)) {
      $info[$file] = array();
    }
    else {
    	
      $data = @file_get_contents($file);
      $category = t("Others");
      
      if(!$categories) $category = "all";
	
      try{
      	$data_parsed = json_decode($data);
      	$data_parsed = $data_parsed ? $data_parsed : new stdClass();
      	
      	$data_parsed->uri = $file;

      	if(isset($data_parsed->category) && $categories){
      		$category = $data_parsed->category;
      	}
      	
      	if(isset($data_parsed->name)){
      		$key = infinity_sanitize($data_parsed->name);
      		$info[$category][$key] = $data_parsed;
      	}else{
      		$dirPath = dirname($file);
      		$dirParts = explode("/", $dirPath);
      		$dirname = end($dirParts);

      		$key = infinity_sanitize($dirname);
      		$data_parsed->name = $dirname;
      		$info[$category][$key] = $data_parsed;
      	}

      }catch(Exception $e){
      	watchdog("Infinity Theme",t("Error while parsing an infinity info file. @msg",array("@msg" => $e->getMessage())));
      }
    }

    return $info;
}

/**
 * Render a custom tpl.php page for our dashboard
 */
function infinity_render_tpl($tpl,$vars = array()){

	$default = array(
		"section" => "",
		"icon" => "",
		"title" => "",
		"classes" => ""
	);

	$vars = array_merge($default,$vars);

	if (is_array($vars) && !empty($vars)) {
		extract($vars);
	}

	if(!file_exists($tpl)) {
		drupal_set_message(t("@tpl not found",array("@tpl" => $tpl)),"error");
		return "";
	}

	$output = "";
	ob_start();
	include($tpl);
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}

/**
 * List out the sections for the dashboard
 */
function infinity_dashboard_sections(){
	$sections = array(
			"grids" => array(
					"title" => 	"Grid",
					"icon" =>	"th"),
			"colors" => array(
					"title" => 	"Colors",
					"icon" =>	"tint"),
			"plugins" => array(
					"title" => 	"Plugins",
					"icon" =>	"cogs"),
			"typography" => array(
					"title" => 	"Typography",
					"icon" =>	"font"),
			"icons" => array(
					"title" => 	"Icons",
					"icon" =>	"smile"),
			"custom" => array(
					"title" => 	"Custom css/js",
					"icon" =>	"code"),
			"profiles" => array(
					"title" => 	"Profiles",
					"icon" =>	"drive")
	);

	return $sections;
}

/**
 * Sanitize string
 */
function infinity_sanitize($string){

	$replace = array(
		" ","/","\\",",",".","(",")"
	);
	
	$string = strtolower($string);
	
	foreach($replace as $symbol){
		$string = str_replace($symbol, "_", $string);
	}	

	return $string;
}


function infinity_ajax_callback($form,$form_state){
	$output = "";
	
	$output .= infinity_less_compile();
	
	$output = '<div id="ajax-wrapper">'.$output."</div>";
	return $output;
}

/**
 * We prepare the files to include on hook_html_preprocess()
 */
function infinity_prepare_files(){
	$theme 			= "infinity";
	$path 			= drupal_get_path('theme', $theme);
	$settings 		= variable_get("theme_settings_".$theme, array());
	
	$components = infinity_scan_components($theme,false);
	$frameworks = isset($settings['grids']) ? $settings['grids'] : array(); 
	$plugins 	= isset($settings['plugins']) ? $settings['plugins'] : array();
	
	$css = array();
	$js = array();
	
	// We need to get the list and uri of all the files that need to be included
	// First the frameworks
	foreach($frameworks as $framework => $status){
		$component = $components['frameworks']['all'][$framework];
		$files = isset($component->files) ? $component->files : array();
		
		if(!$files){
			drupal_set_message(t("No files found for the component <strong>@component</strong>. Please check the infinity plugin file.",array("@component"=>$component->name)),"error");
		}
		foreach($files as $file){
			
			if(strpos($file, "theme://") !== false){
				$file = str_replace("theme://", $path."/", $file);
			}else{
				$dir = dirname($component->uri);
				$file = $dir . "/" . $file;
			}
			
			if(file_exists($file) && !in_array($file, $css)) {
				$css[] = $file;
			}else{
				drupal_set_message(t("The file <strong>@file</strong> of component <strong>@component</strong> have not been found.",array("@file" => $file,"@component"=>$component->name)),"error");
			}
		}
	}
	
	// Then the plugins
	foreach($plugins as $plugin => $status){
		$component = $components['plugins']['all'][$plugin];
		$files = isset($component->files) ? $component->files : array();
	
		if(!$files){
			drupal_set_message(t("No files found for the component <strong>@component</strong>. Please check the infinity plugin file.",array("@component"=>$component->name)),"error");
		}
		foreach($files as $file){
				
			if(strpos($file, "theme://") !== false){
				$file = str_replace("theme://", $path."/", $file);
			}else{
				$dir = dirname($component->uri);
				$file = $dir . "/" . $file;
			}
				
			if(file_exists($file) && !in_array($file, $css)) {
				$js[] = $file;
			}else{
				drupal_set_message(t("The file <strong>@file</strong> of component <strong>@component</strong> have not been found.",array("@file" => $file,"@component"=>$component->name)),"error");
			}
		}
	}
	
	// If the typography is set we need to add webfontloader.js
	if(isset($settings["typography"]) && count($settings["typography"]) != 0){
		$js[] = $path . '/core/third_party/helpers/webfontloader/webfontloader.js';
	}
	
	// If the icons are enabled, add them too 
	if(isset($settings['icons']) && isset($settings['icons']->enabled)){
		$css[] = $path . '/core/fonts/gb-font/gb-font.css';
	}
	
	$includes = array(
		"css" => $css,
		"js"  => $js
	);
	
	variable_set("theme_settings_".$theme."_files", $includes);
}

/**
 * Basically it compiles the default style.less but can compile other files as well
 */
function infinity_less_compile($file = null){
	
	$base 	= $GLOBALS["base_url"];
	$theme 	= "infinity";
	$path 	= drupal_get_path('theme', $theme);
	$less 	= DRUPAL_ROOT."/".$path."/core/third_party/helpers/lessphp2/lib/Less/Autoloader.php";
	$style  = DRUPAL_ROOT."/".$path."/less/styles.less";
	
	if(!$file){
		$file = $style;
	}else{
		// We replace the uri keywords theme:// by their respective paths
		$file = str_replace("theme://",DRUPAL_ROOT."/".$path."/");
	}
	
	require_once($less);
	Less_Autoloader::register();
	
	$options = array(
			'compress' => true,
			'cache_dir'=>DRUPAL_ROOT."/".$path."/less/cache"
		);
	
	$parser = new Less_Parser($options);
	
	$css = "";
	
	try{
		$parser->parseFile($style,$base."/");
		$css = $parser->getCss();
	}catch(Exception $e){
		drupal_set_message('<h3>Failed Compilation</h3>'.$e->getMessage(),"error");
	}
	
	return $css;	
}
/**
 * Return a custom file which is either custom.color.less, custom.typography.less, custom.less
 * 
 */
function infinity_get_custom_file($file_path){
	
	$content = "";
	
	if(file_exists($file_path)){
		$content = file_get_contents($file_path);
	}
	
	return $content;
}

/**
 * Create the colors variables file
 */
function infinity_create_colors_variables($colors){
	
	$output = "";
	if(!$colors) $colors = array();
	foreach($colors as $hex => $color){
		$output .= "@".$color . ": #" . $hex .";\n";	
		$output .= "@".$color . "_name: ~'".$color."';\n";
		$output .= ".generate-color(@".$color.",@".$color."_name);\n";
	}
	
	
	$theme 	= "infinity";
	$path 	= drupal_get_path('theme', $theme);
	file_put_contents(DRUPAL_ROOT."/".$path."/less/colors/variables.colors.less", $output);
}

/**
 * Create the colors variables file
 */
function infinity_create_fonts_variables($fonts){

	$output = "";
	if(!$fonts) $fonts = array();
	foreach($fonts as $key_name => $font){
		
		$fontname = explode(":",$font);
		$fontname = $fontname[0];
		
		$output .= "@".$key_name . ": '" . $fontname ."';\n";
		$output .= ".txt-".$key_name."{font-family:@".$key_name.";}\n";
	}
	
	$theme 	= "infinity";
	$path 	= drupal_get_path('theme', $theme);
	file_put_contents(DRUPAL_ROOT."/".$path."/less/typography/variables.typography.less", $output);
}

/**
 * Create the js loader that will be used to load the fonts
 */
function infinity_create_js_loader($fonts){
	
	if(!$fonts){
		file_put_contents(DRUPAL_ROOT."/".$path."/js/infinity.typography.js", "");
	}
	
	$output = "(function ($) {
	
  		Drupal.behaviors.infinityThemeTypographyLoadBehavior = {
    		attach: function (context, settings) {";
	
	$families = array();
	
	foreach($fonts as $key_name => $font){
		$families[] = "'".$font."'";		
	}
	
	$families = implode(",", $families);
	$families = "[".$families."]";
	
	$output .= 'WebFont.load({
			    google: {
			      families: '.$families.'
			    }
			});';
	
	$theme 	= "infinity";
	$path 	= drupal_get_path('theme', $theme);
	
	$output .=  "}
			  };
			
			})(jQuery);";
	
	file_put_contents(DRUPAL_ROOT."/".$path."/js/infinity.typography.js", $output);
}

/**
 * Save the current profile in a file
 */
function infinity_save_profile($theme,$profile = 'default'){
	$path 		= drupal_get_path('theme', $theme);
	$settings 	= variable_get("theme_settings_".$theme, array());
	
	// We do not forget the custom files
	$custom_color = infinity_get_custom_file(DRUPAL_ROOT."/".$path."/less/colors/custom.colors.less");
	$custom_typography = infinity_get_custom_file(DRUPAL_ROOT."/".$path."/less/typography/custom.typography.less");
	$custom_css = infinity_get_custom_file(DRUPAL_ROOT."/".$path."/less/custom.less");
	$custom_js = infinity_get_custom_file(DRUPAL_ROOT."/".$path."/js/custom.js");
	
	$settings["custom_color"] = $custom_color;
	$settings["custom_typography"] = $custom_typography;
	$settings["custom_css"] = $custom_css;
	$settings["custom_js"] = $custom_js;
	
	// Now we stringify all of this ... 
	$settings["name"] = $profile;
	
	$profile_content = json_encode($settings);
	$profile_file_name = infinity_sanitize($profile);
	file_put_contents(DRUPAL_ROOT."/".$path."/core/profiles/".$profile_file_name.'.profile', $profile_content);
}

/**
 * Delete a profile
 */
function infinity_delete_profile($theme,$profile){
	$path 		= drupal_get_path('theme', $theme);
	$settings 	= variable_get("theme_settings_".$theme, array());
	
	$profile_file_name = infinity_sanitize($profile);
	@unlink(DRUPAL_ROOT."/".$path."/core/profiles/".$profile_file_name.'.profile');
}

/**
 * Load Profile
 */
function infinity_load_profile($theme,$profile){
	$path 		= drupal_get_path('theme', $theme);
	$settings 	= variable_get("theme_settings_".$theme, array());
	
	$profile_file_name = infinity_sanitize($profile);
	$file = DRUPAL_ROOT."/".$path."/core/profiles/".$profile_file_name.'.profile';
	
	$profile_data = infinity_get_custom_file($file);
	
	if($profile_data){
		$data = json_decode($profile_data);
		$data = (array)$data;
	}else{
		$data = array();
	}
	
	return $data;
}