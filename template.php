<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * infinity theme.
 */

function infinity_preprocess_html(){
	$theme 			= $GLOBALS['theme_key'];
	$path 			= drupal_get_path('theme', $theme);
	
	$files = variable_get("theme_settings_".$theme."_files", array());
	
	if(isset($files["css"])){
		foreach($files['css'] as $css){
			drupal_add_css($css);
		}	
	}
	
	if(isset($files['js'])){
		foreach($files['js'] as $js){
			drupal_add_js($js);
		}	
	}
	
}