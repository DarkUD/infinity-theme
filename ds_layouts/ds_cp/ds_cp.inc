<?php
function ds_ds_cp() {
  return array(
    'label' => t('CP Content Page'),
    'regions' => array(
      	'banner' => t('Banner'),
      	'breadcrumb' => t('Breadcrumb'),
      	'social' => t('Social'),
      	'ad' => t('Ad top'),
      	'menu' => t('Menu'),
      	'middle' => t('Content'),
    	'comments' => t('Comments'),
    	'sidebar' => t("Sidebar (right)"),
      	'ad_bottom' => t("Ad Bottom"),
      	'slots' => t('Slots'),
      	'block_grid' => t('Block Grid'),
    	'footer' => t('Footer'),
    ),
    // Add this line if there is a default css file.
    'css' => TRUE,
    // Add this line if you're using DS 2.x for icon preview
    'image' => TRUE,
  );
}
