<?php

/**
 * @file
 * Content page layout
 */


$gb_ctxt_settings = variable_get("gb_ctxt_configuration_settings",array());
$host_entity = null;

if(isset($elements["#node"])){
	$node = $elements['#node'];
	$host_entity = $node;
	$nid = $node->nid;
	$current_context = _gb_ctxt_get_page_context($node->nid);
	
	if(!$current_context) $current_context = "default";
	
	$node_ctxt = isset($node->node_ctxts[$current_context]) ? $node->node_ctxts[$current_context] : null;
	$default_ctxt = isset($node->node_ctxts["default"]) ? $node->node_ctxts["default"] : null;
	
	// We load the ctxt fields
	$ctxt_title = array();
	$subtitle = array();
	$pre_content = array();
	$description = array();
	$pre_footer = array();
	$post_footer = array();
	$logos = array();
	$banners = array();
	
	if($default_ctxt){
		$ctxt_title[] = $default_ctxt->field_context_title->value();
		if($default_ctxt->field_context_subtitle->value()) $subtitle[] = $default_ctxt->field_context_subtitle->value();
		$pre_content[] = $default_ctxt->field_context_pre_content->value();
		$description[] = $default_ctxt->field_context_description->value();
		$pre_footer[] = $default_ctxt->field_context_pre_footer->value();
		$post_footer[] = $default_ctxt->field_context_post_footer->value();
		if($default_ctxt->field_context_logo->value()) $logos[] = $default_ctxt->field_context_logo->value();
		if($default_ctxt->field_context_banner->value()) $banners[] = $default_ctxt->field_context_banner->value();
	}
	
	if($node_ctxt){
		$ctxt_title[] = $node_ctxt->field_context_title->value();
		if($node_ctxt->field_context_subtitle->value()) $subtitle[] = $node_ctxt->field_context_subtitle->value();
		$pre_content[] = $node_ctxt->field_context_pre_content->value();
		$description[] = $node_ctxt->field_context_description->value();
		$pre_footer[] = $node_ctxt->field_context_pre_footer->value();
		$post_footer[] = $node_ctxt->field_context_post_footer->value();
		if($node_ctxt->field_context_logo->value()) $logos[] = $node_ctxt->field_context_logo->value();
		if($node_ctxt->field_context_banner->value()) $banners[] = $node_ctxt->field_context_banner->value();
	}
	
	$node_menu = _gb_ctxt_generate_menu($node);
	
// 	$desc = end($description);
// 	$form = drupal_get_form("_gb_ctxt_get_inline_edit_form",$node_ctxt,$host_entity,"field_context_description");
// 	print drupal_render($form);
	
}

// Load the terms
if(isset($elements["#term"])){
	$term = $elements['#term'];
	$host_entity = $term;
	$tid = $term->tid;
	$current_context = _gb_ctxt_get_taxonomy_page_context($tid);
	
	if(!$current_context) $current_context = "default";
	
	$node_ctxt = isset($term->term_ctxts[$current_context]) ? $term->term_ctxts[$current_context] : null;
	$default_ctxt = isset($term->term_ctxts["default"]) ? $term->term_ctxts["default"] : null;
	
	// We load the ctxt fields
	$ctxt_title = array();
	$subtitle = array();
	$pre_content = array();
	$description = array();
	$pre_footer = array();
	$post_footer = array();
	$logos = array();
	$banners = array();
	
	if($default_ctxt){
		$ctxt_title[] = $default_ctxt->field_context_title->value();
		if($default_ctxt->field_context_subtitle->value()) $subtitle[] = $default_ctxt->field_context_subtitle->value();
		$pre_content[] = $default_ctxt->field_context_pre_content->value();
		$description[] = $default_ctxt->field_context_description->value();
		$pre_footer[] = $default_ctxt->field_context_pre_footer->value();
		$post_footer[] = $default_ctxt->field_context_post_footer->value();
		if($default_ctxt->field_context_logo->value()) $logos[] = $default_ctxt->field_context_logo->value();
		if($default_ctxt->field_context_banner->value()) $banners[] = $default_ctxt->field_context_banner->value();
	}
	
	if($node_ctxt){
		$ctxt_title[] = $node_ctxt->field_context_title->value();
		if($node_ctxt->field_context_subtitle->value()) $subtitle[] = $node_ctxt->field_context_subtitle->value();
		$pre_content[] = $node_ctxt->field_context_pre_content->value();
		$description[] = $node_ctxt->field_context_description->value();
		$pre_footer[] = $node_ctxt->field_context_pre_footer->value();
		$post_footer[] = $node_ctxt->field_context_post_footer->value();
		if($node_ctxt->field_context_logo->value()) $logos[] = $node_ctxt->field_context_logo->value();
		if($node_ctxt->field_context_banner->value()) $banners[] = $node_ctxt->field_context_banner->value();
	}
	
	$node_menu = _gb_ctxt_generate_menu($term,"taxonomy_term");	
}

$debug = " debug";
$title = drupal_static("gb_ctxt_page_title","");

if(($middle && $comments)  || ($description && $comments)){
	$classes .= " content-and-comments";
}

$has_logo = !empty($logos);
$has_banner = !empty($banners);

if($sidebar){
	$classes .= " have-sidebar";
}

if(!$banner && !$has_logo && !$has_banner){
	$classes .= " no-banner";
}

$inline_edit = "";

if(user_access("inline edit contexts")){
	drupal_add_js(drupal_get_path("module", "gb_ctxt")."/assets/js/gb-ctxt-inline-edit.js");
	drupal_add_css(drupal_get_path("module", "gb_ctxt")."/assets/css/gb-ctxt-inline-edit.css");
	
	drupal_add_js(libraries_get_path("ckeditor-inline-edit")."/ckeditor.js");	
// 	drupal_add_css(libraries_get_path("ckeditor-inline-edit")."/ckeditor.css");
	$inline_edit = " inline-editable";
}

// dpm($logos);
// dpm($banner);

drupal_add_css(drupal_get_path("theme", "radiohub")."/ds_layouts/ds_cp/gb-sass/css/ds_cp.css");

?>

<<?php print $layout_wrapper; print $layout_attributes; ?> class="content-page-layout <?php print $classes;?> clearfix<?php print $debug;?>">

<?php if(!$banner && !$has_logo && !$has_banner):?>
	<h1 id="page--title" class="<?php print $inline_edit?>"><div class="value"<?php if($inline_edit):?> contenteditable="true" noformat textfield<?php endif;?>><?php print $title?></div><?php if(user_access("inline edit contexts") && $node_ctxt){
	  		$form = drupal_get_form("_gb_ctxt_get_inline_edit_form",$node_ctxt,$host_entity,"field_context_page_title");
	  		print drupal_render($form);
	  	}?></h1>
	  	
	 <?php if(end($subtitle)):?>
    	<h2 class="ctxt-subtitle<?php print $inline_edit?>"><div class="value"<?php if($inline_edit):?> contenteditable="true" noformat textfield<?php endif;?>><?php print end($subtitle)?></div>
    	<?php if(user_access("inline edit contexts") && $node_ctxt){
    		$form = drupal_get_form("_gb_ctxt_get_inline_edit_form",$node_ctxt,$host_entity,"field_context_subtitle");
    		print drupal_render($form);
    	}?>
    	</h2>
    <?php endif;?>
    
<?php endif;?>

<?php if($banner || $has_logo || $has_banner):?>
  <<?php print $banner_wrapper ?> class="group-banner<?php print $banner_classes; ?>">
  	<h1 id="page--title" class="<?php print $inline_edit?>"><div class="value"<?php if($inline_edit):?> contenteditable="true" noformat textfield<?php endif;?>><?php print $title?></div><?php if(user_access("inline edit contexts") && $node_ctxt){
		$form = drupal_get_form("_gb_ctxt_get_inline_edit_form",$node_ctxt,$host_entity,"field_context_page_title");
		print drupal_render($form);
	}?></h1>
  	
    <?php if($has_banner || $has_logo ):?>
    
    <div class="gb-ctxt-logo-and-banner">
    
	    <?php if(end($banners)):?>
	    	<div class="ctxt-banner">
	    	<?php 
	    		$img = end($banners);
	    		$image_style = $gb_ctxt_settings['banner_style'];
	    		
	    		$build = array(
	    				'#theme' => "image",
	    				'#style_name' => $image_style,
	    				"#path" => $img['uri']
	    		);
	    		
	    		if($image_style != "none") {
	    			$build["#theme"] = "image_style";
	    		}
	    		
	    		print render($build);    		
	    	?>
	        </div>
	    <?php endif;?>
	   
	    <?php if(end($logos)):?>
	    	<div class="ctxt-logo">
	    	<?php 
	    		$logo = end($logos);
	    		$image_style = $gb_ctxt_settings['logo_style'];
	    		
	    		$build = array(
					'#theme' => "image",    			
					'#style_name' => $image_style,
					"#path" => $logo['uri']
	    		);
	    		
	    		if($image_style != "none") {
	    			$build["#theme"] = "image_style";
	    		}
	
				print render($build);
	    	?>
	        </div>
	    <?php endif;?>
   
   </div>
   
   <?php endif?>
   
   <?php if($banner):?>
	   <div class="gb-ctxt-banner-content">
	    <?php print $banner; ?>
	   </div>
   <?php endif;?>
   
   <?php if(end($subtitle)):?>
	  	<h2 class="ctxt-subtitle<?php print $inline_edit?>"><div class="value"<?php if($inline_edit):?> contenteditable="true" noformat textfield<?php endif;?>><?php print end($subtitle)?></div>
		<?php if(user_access("inline edit contexts") && $node_ctxt){
			$form = drupal_get_form("_gb_ctxt_get_inline_edit_form",$node_ctxt,$host_entity,"field_context_subtitle");
			print drupal_render($form);
		}?>
			</h2>
    <?php endif;?>
   
  </<?php print $banner_wrapper ?>>
<?php endif;?>

  <?php if ($ad): ?>
    <<?php print $ad_wrapper ?> class="group-ad<?php print $ad_classes; ?>">
      <?php print $ad; ?>
    </<?php print $ad_wrapper ?>>
  <?php endif; ?>

  <<?php print $breadcrumb_wrapper ?> class="group-breadcrumb<?php print $breadcrumb_classes; ?>">
      <?php print $breadcrumb; ?>
      <?php print theme("breadcrumb",array("breadcrumb" => drupal_static("gb_ctxt_breadcrumb",array())))?>
    </<?php print $breadcrumb_wrapper ?>>

  <?php if ($social): ?>
    <<?php print $social_wrapper ?> class="group-social<?php print $social_classes; ?>">
      <?php print $social; ?>
    </<?php print $social_wrapper ?>>
  <?php endif; ?>

  <div class="content-holder">
  	<div class="main-holder">
  	   
  	   <?php if ($menu || $node_menu): ?>
    		<<?php print $menu_wrapper ?> class="group-menu<?php print $menu_classes; ?>">
      		<?php print $menu; ?>
      		<?php print $node_menu;?>
    		</<?php print $menu_wrapper ?>>
	  <?php endif; ?>

	  <?php if ($middle || end($ctxt_title) || end($description) || end($pre_content)): ?>
	    	<<?php print $middle_wrapper ?> class="group-middle<?php print $middle_classes; ?>">
	      	
				      	<?php if(end($pre_content)):?>
				      		<?php $desc = end($pre_content);?>
				      		
					      	<div class="ctxt-pre-content<?php print $inline_edit?>">
						      	<div class="value"<?php if($inline_edit):?> contenteditable="true"<?php endif;?> <?php if(!isset($desc['format'])):?> noformat="true"<?php endif;?>>
						    <?php 
					      	 
					      	if(isset($desc['value'])){
					      		print $desc['value'];
					      	}else{
					      		print check_plain($desc);
					      	}
					      	
					      	?></div>
					      	<div class="gb-ctxt-inline-edit-trigger">
					      		<?php if(user_access("inline edit contexts") && $node_ctxt){
			    					$form = drupal_get_form("_gb_ctxt_get_inline_edit_form",$node_ctxt,$host_entity,"field_context_pre_content");
			    					print drupal_render($form);
					    		}?>
					    	</div>
					      </div>
			      <?php endif;?>
	      	
	      	<?php if(end($ctxt_title) || end($description)):?>
	      		<?php 
	      			$classes = array();
	      			if(end($ctxt_title)) $classes[] = "have-ctxt-title";
	      			if(end($description)) $classes[] = "have-ctxt-desc";
	      		?>
	      		<div <?php print drupal_attributes(array("class"=>$classes));?>>
	      			<?php if(end($ctxt_title)):?>
	      				<h3 class="ctxt-title<?php print $inline_edit?>">
	      					<div class="value" <?php if($inline_edit):?> contenteditable="true" noformat textfield<?php endif;?>>
	      						<?php print check_plain(end($ctxt_title))?>
	      					</div>
	      					<div class="gb-ctxt-inline-edit-trigger">
				      		<?php if(user_access("inline edit contexts") && $node_ctxt){
		    					$form = drupal_get_form("_gb_ctxt_get_inline_edit_form",$node_ctxt,$host_entity,"field_context_title");
		    					print drupal_render($form);
				    		}?>
	      				</h3>
	      			<?php endif;?>
	      			
	      			<?php if(end($description)):?>
	      			<?php $desc = end($description);?>
      					<div class="ctxt-desc<?php print $inline_edit?>">
	      				<div class="value" <?php if($inline_edit):?> contenteditable="true"<?php endif;?> <?php if(!isset($desc['format'])):?> noformat="true"<?php endif;?>>
	      				<?php 
	      				      				
	      				if(isset($desc['value'])){
							print $desc['value'];			
						}else{
							print check_plain($desc);
						}
						?>
	      				</div>
	      				<div class="gb-ctxt-inline-edit-trigger">
			      		<?php if(user_access("inline edit contexts") && $node_ctxt){
	    					$form = drupal_get_form("_gb_ctxt_get_inline_edit_form",$node_ctxt,$host_entity,"field_context_description");
	    					print drupal_render($form);
			    		}?>
			    		</div>
	      				</div>
	      			<?php endif;?>
	      		</div>
	      	<?php endif;?>      	
	      	
	      	<?php print $middle; ?>
	    	</<?php print $middle_wrapper ?>>
	  <?php endif; ?>
  
	  <?php if ($comments): ?>
	    	<<?php print $comments_wrapper ?> class="group-comments<?php print $comments_classes; ?>">
	      	<?php print $comments; ?>
	    	</<?php print $comments_wrapper ?>>
	  <?php endif; ?>
  	</div>

  <?php if ($sidebar): ?>
    <<?php print $sidebar_wrapper ?> class="group-sidebar<?php print $sidebar_classes; ?>">
      <?php print $sidebar; ?>
    </<?php print $sidebar_wrapper ?>>
  <?php endif; ?>
  </div>
  
  <?php if ($slots): ?>
    <<?php print $slots_wrapper ?> class="group-slots<?php print $slots_classes; ?>">
      <?php print $slots; ?>
    </<?php print $slots_wrapper ?>>
  <?php endif; ?>

  <?php if ($block_grid): ?>
    <<?php print $block_grid_wrapper ?> class="group-block-grid<?php print $block_grid_classes; ?>">
      <?php print $block_grid; ?>
    </<?php print $block_grid_wrapper ?>>
  <?php endif; ?>

  <?php if ($footer || end($pre_footer) || end($post_footer)): ?>
    <<?php print $footer_wrapper ?> class="group-footer<?php print $footer_classes; ?>">
      <?php if(end($pre_footer)):?>
      	<div class="ctxt-pre-footer<?php print $inline_edit?>">
	      	<div class="value" <?php if($inline_edit):?> contenteditable="true"<?php endif;?> <?php if(!isset($desc['format'])):?> noformat="true"<?php endif;?>>
	      	<?php 
      	
		      	$desc = end($pre_footer);
		      	 
		      	if(isset($desc['value'])){
		      		print $desc['value'];
		      	}else{
		      		print check_plain($desc);
		      	}
      	
      		?></div>
      	<div class="gb-ctxt-inline-edit-trigger">
      		<?php if(user_access("inline edit contexts") && $node_ctxt){
    					$form = drupal_get_form("_gb_ctxt_get_inline_edit_form",$node_ctxt,$host_entity,"field_context_pre_footer");
    					print drupal_render($form);
    		}?>
    	</div>
      </div>
      <?php endif;?>
      
      <?php print $footer; ?>
      
      <?php if(end($post_footer)):?>
      	<div class="ctxt-post-footer<?php print $inline_edit?>">
	      	<div class="value" <?php if($inline_edit):?> contenteditable="true"<?php endif;?> <?php if(!isset($desc['format'])):?> noformat="true"<?php endif;?>>
	      	<?php 
		      	$desc = end($post_footer);
		      	
		      	if(isset($desc['value'])){
		      		print $desc['value'];
		      	}else{
		      		print check_plain($desc);
		      	}
	     	?>
     		</div>
     	<div class="gb-ctxt-inline-edit-trigger">
      		<?php if(user_access("inline edit contexts") && $node_ctxt){
    					$form = drupal_get_form("_gb_ctxt_get_inline_edit_form",$node_ctxt,$host_entity,"field_context_post_footer");
    					print drupal_render($form);
    		}?>
    	</div>
     	</div>
      <?php endif;?>
    </<?php print $footer_wrapper ?>>
  <?php endif; ?>

  </div>

<?php if ($ad_bottom): ?>
    <<?php print $ad_bottom_wrapper ?> class="group-ad-bottom<?php print $ad_bottom_classes; ?>">
      <?php print $ad_bottom; ?>
    </<?php print $ad_bottom_wrapper ?>>
  <?php endif; ?>
  
  </<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>


<?php if(user_access("inline edit contexts")):?>

<?php endif;?>