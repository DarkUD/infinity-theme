<?php

/**
 * @file
 * Display Suite fluid 3 column 25/50/25 stacked template.
 */

  // Add sidebar classes so that we can apply the correct width to the center region in css.
  if (($left && !$right) || ($right && !$left)) $classes .= ' group-one-sidebar';
  if ($left && $right) $classes .= ' group-two-sidebars';
  if ($left) $classes .= ' group-sidebar-left';
  if ($right) $classes .= ' group-sidebar-right';


?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="gb-radio <?php print $classes;?> clearfix">

  <<?php print $header_wrapper ?> class="group-header<?php print $header_classes; ?>">
	    <?php print $header; ?>
  </<?php print $header_wrapper ?>>

  <div class="group-sub-header">
  <?php if($breadcrumb || $social_top):?>
  <?php if ($breadcrumb): ?>
    <<?php print $breadcrumb_wrapper ?> class="group-breadcrumb<?php print $breadcrumb_classes; ?>">
      <?php print $breadcrumb; ?>
    </<?php print $breadcrumb_wrapper ?>>
  <?php endif; ?>

  <?php if ($social_top): ?>
    <<?php print $social_top_wrapper ?> class="group-social_top<?php print $social_top_classes; ?>">
      <?php print $social_top; ?>
    </<?php print $social_top_wrapper ?>>
  <?php endif; ?>
  </div>

  <?php endif;?>

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

   <?php if ($pub): ?>
    <<?php print $pub_wrapper ?> class="group-pub<?php print $pub_classes; ?>">
      <?php print $pub; ?>
    </<?php print $pub_wrapper ?>>
  <?php endif; ?>

  <div id="radio-content">

  <?php if ($middle || $menu): ?>
    <<?php print $middle_wrapper ?> class="group-middle<?php print $middle_classes; ?>">
        <div id="radio-main">
         <?php print $middle; ?>
        </div>
       <?php if ($menu): ?>
	    <<?php print $menu_wrapper ?> class="group-menu<?php print $menu_classes; ?>">
	       <?php print $menu; ?>
	    </<?php print $menu_wrapper ?>>
	  <?php endif; ?>

    </<?php print $middle_wrapper ?>>
  <?php endif; ?>

  <?php if ($left): ?>
    <<?php print $left_wrapper ?> class="group-left<?php print $left_classes; ?>">
      <?php print $left; ?>
    </<?php print $left_wrapper ?>>
  <?php endif; ?>

  <?php if ($right): ?>
    <<?php print $right_wrapper ?> class="group-right<?php print $right_classes; ?>">
      <?php print $right; ?>
    </<?php print $right_wrapper ?>>
  <?php endif; ?>

  </div>

  <<?php print $footer_wrapper ?> class="group-footer<?php print $footer_classes; ?>">
    <?php print $footer; ?>
  </<?php print $footer_wrapper ?>>

</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
