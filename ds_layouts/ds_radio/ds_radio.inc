<?php
function ds_ds_radio() {
  return array(
    'label' => t('Ds Radio'),
    'regions' => array(
      'header' => t('Header'),
      'breadcrumb' => t('Breadcrumb'),
      'social_top' => t('Social Top'),
      'pub' => t('Pub'),
      'menu' => t('Menu'),
      'left' => t('Left'),
      'middle' => t("Middle"),
      'footer' => t("Footer"),
      'right' => t('Right'),
      'Social_bottom' => t('Social_bottom'),
    ),
    // Add this line if there is a default css file.
    'css' => TRUE,
    // Add this line if you're using DS 2.x for icon preview
    'image' => TRUE,
  );
}
