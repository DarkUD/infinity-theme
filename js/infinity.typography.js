(function ($) {
	
  		Drupal.behaviors.infinityThemeTypographyLoadBehavior = {
    		attach: function (context, settings) {WebFont.load({
			    google: {
			      families: ['Open sans','Droid Sans','Montserrat']
			    }
			});}
			  };
			
			})(jQuery);