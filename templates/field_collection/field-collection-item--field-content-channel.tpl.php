<?php

/**
 * @file
 * Default theme implementation for field collection items.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) field collection item label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-field-collection-item
 *   - field-collection-item-{field_name}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

$wrapper = entity_metadata_wrapper("field_collection_item",$elements['#entity']);
$sticky = $wrapper->field_sticky->value();
$url = $wrapper->field_url->value();

if(!$sticky){
	hide($content['field_image']);
	hide($content['field_description']);
}

$type = $wrapper->field_type->value();

$icon = "";

if($type){
	$w_term = entity_metadata_wrapper("taxonomy_term",$type);
	$icon = $w_term->field_icon->value();	
}


?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <?php if($url):?>
    	<a class="tile-link" href="<?php print $url; ?>"></a>
    <?php endif;?>
    <div class="content-label">
    	<span class="g-icon g-icon-<?php print $icon;?>"></span>
    	<?php print render($content['field_label'])?>
    </div>
    <?php      
    	print render($content);
    ?>
  </div>
</div>