<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['branding']: Items for the branding region.
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see omega_preprocess_page()
 */
?>
<div class="l-page">
  <header class="l-header" role="banner">
    <div class="l-sub-header">
    	<div class="container l-mts l-branding-mobile">
		    <div class="l-branding">
		      <?php if ($logo): ?>
		        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print _radiohub_get_logo() ?>" alt="<?php print t('Home'); ?>" /></a>
		      <?php endif; ?>
		
		      <?php //if ($site_name || $site_slogan): ?>
		      <?php if(false): ?>
		        <?php if ($site_name): ?>
		          <h1 class="site-name">
		            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
		          </h1>
		        <?php endif; ?>
		
		        <?php if ($site_slogan): ?>
		          <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
		        <?php endif; ?>
		      <?php endif; ?>
		
		      <?php print render($page['branding']); ?>
		    </div>
		    
		    <div class="mobile-btns">
			    <span class="mobile-btn btn-search"><span class="g-icon g-icon-search"></span></span>
			    <span class="mobile-btn btn-menu"><span class="g-icon g-icon-menu2"></span></span>
		    </div>
		    
		    <?php print render($page['header']); ?>	    
		</div>	
		    
		    <?php print render($page['search']); ?>
		    <?php print render($page['navigation']); ?>
		    
    </div>
    <div class="l-radio-header">
    	<div class="container l-mts clearfix">
    		<?php print render($page['radio']); ?>
    		<?php print render($page['favorites']); ?>
    	</div>
    </div>
  </header>
  <div class="l-main clearfix">  
    <div class="l-content" role="main">
    <div id="matrix-wrapper">
    	<canvas id="c"></canvas>
    </div>
      <?php print render($page['highlighted']); ?>
      <?php print $breadcrumb; ?>
      <a id="main-content"></a>
          
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      
      	<div id="error-page-msg">	
    	 <h1>-404-</h1>
     	 <?php if ($title): ?>
	     <h2><?php print $title; ?></h2>
	     <?php endif; ?>
     	 <?php print render($page['content']); ?>
      	</div>
      <style>
      	#error-page-msg{
      		position:absolute;
      		top:40%;
      		left:0;
      		right:0;
      		background:rgba(255,255,255,0.5);
      		border-top:3px solid #ccc;
      		border-bottom:3px solid #ccc;
      		z-index:10;
      		text-align:center;
      		padding-bottom:20px;	
      	}
      	
      	.l-main{
      		position:relative;
      	}
      	
      	.l-content{
      		position:relative;
      		overflow:hidden;      		
      		min-height:500px;     		
      	}
      	
      	.l-footer{
      		position:relative;
      		z-index:1;
      		margin-top:0;
      	}	
      	
      	#matrix-wrapper{
      		position:absolute;
      		top:0;
      		left:0;
      		bottom:0;
      		right:0; 
      		width:100%;
      		height:100%;
      		overglow:hidden;
      		display:block;
      		z-index: 0;   		
      	}
      	
      </style>
      
      
      <script>
      (function ($, Drupal, window, document, undefined) {
              	  	 
    	  	  var c = document.getElementById("c");
    	  	  var wrapper = document.getElementById("matrix-wrapper");
    	  	  var ctx = c.getContext("2d");
    	  	  
    	  	  c.height = window.innerHeight + 200;
    	  	  c.width = window.innerWidth;
    	  	  
    	  	  var chinese = "田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑";
    	  	  
    	  	  chinese = chinese.split("");
    	  	  
    	  	  var font_size = 10;
    	  	  var columns = c.width/font_size;
    	  	  
    	  	  var drops = [];
    	  	  
    	  	  for(var x = 0;x < columns ; x++){
    	  		  drops[x] = 1;
    	  	  }
    	  	  
    	  	  function draw(){
    	  		  
    	  		  ctx.fillStyle = "rgba(255,255,255,0.05)";
    	  		  ctx.fillRect(0,0,c.width,c.height);
    	  		  
    	  		  ctx.fillStyle = "#0F0";
    	  		  ctx.font = font_size + "px arial";
    	  		  
    	  		  for(var i = 0;i < drops.length ; i++){
    	  			 var text = chinese[Math.floor(Math.random()*chinese.length)];
    	  			 ctx.fillText(text,i*font_size,drops[i]*font_size);
    	  			 
    	  			 if(drops[i] * font_size> c.height && Math.random() > 0.975) drops[i] = 0;
    	  			 
    	  			 drops[i]++;
    	  		  }
    	  	  }
    	  	  
    	  	  setInterval(draw,33);
	    
    	  })(jQuery, Drupal, this, this.document);

      </script>
      <?php print $feed_icons; ?>
    </div>

    <?php print render($page['sidebar_first']); ?>
    <?php print render($page['sidebar_second']); ?>
  </div>  
</div>

<footer class="l-footer" role="contentinfo">
	
	<?php print render($page['footer_1']); ?>
	<?php print render($page['footer_2']); ?>
	<?php print render($page['footer_3']); ?>
	<?php print render($page['footer_4']); ?>
    <?php print render($page['footer']); ?>
</footer>
<div class="clearfix"></div>
<div id="copyright">© <?php print Date("Y")?> <strong><?php print variable_get("site_name","") ?></strong> <?php print t("All rights reserved")?></div>
