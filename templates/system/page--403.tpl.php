<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['branding']: Items for the branding region.
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see omega_preprocess_page()
 */
?>
<div class="l-page">
  <header class="l-header" role="banner">
    <div class="l-sub-header">
    	<div class="container l-mts l-branding-mobile">
		    <div class="l-branding">
		      <?php if ($logo): ?>
		        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print _radiohub_get_logo() ?>" alt="<?php print t('Home'); ?>" /></a>
		      <?php endif; ?>
		
		      <?php //if ($site_name || $site_slogan): ?>
		      <?php if(false): ?>
		        <?php if ($site_name): ?>
		          <h1 class="site-name">
		            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
		          </h1>
		        <?php endif; ?>
		
		        <?php if ($site_slogan): ?>
		          <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
		        <?php endif; ?>
		      <?php endif; ?>
		
		      <?php print render($page['branding']); ?>
		    </div>
		    
		    <div class="mobile-btns">
			    <span class="mobile-btn btn-search"><span class="g-icon g-icon-search"></span></span>
			    <span class="mobile-btn btn-menu"><span class="g-icon g-icon-menu2"></span></span>
		    </div>
		    
		    <?php print render($page['header']); ?>	    
		</div>	
		    
		    <?php print render($page['search']); ?>
		    <?php print render($page['navigation']); ?>
		    
    </div>
    <div class="l-radio-header">
    	<div class="container l-mts clearfix">
    		<?php print render($page['radio']); ?>
    		<?php print render($page['favorites']); ?>
    	</div>
    </div>
  </header>
  <div class="l-main clearfix">
    <div class="l-content" role="main">
      <?php print render($page['highlighted']); ?>
      <?php print $breadcrumb; ?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
     	<div id="error-page-msg">	
    	 <h1>-403-</h1>
     	 <?php if ($title): ?>
	     <h2><?php print $title; ?></h2>
	     <?php endif; ?>
     	 <?php print render($page['content']); ?>
      	</div>
      
      <style>
      	#error-page-msg{
      		position:absolute;
      		top:50%;
      		left:0;
      		right:0;
      		background:rgba(255,255,255,0.5);
      		border-top:3px solid #ccc;
      		border-bottom:3px solid #ccc;
      		z-index:10;
      		text-align:center;
      		padding-bottom:20px;	
      	}
      
      	.l-main{
      		background-image: linear-gradient(bottom,rgb(69,132,180) 28%,rgb(31,71,120) 64%);
			background-image: -o-linear-gradient(bottom,rgb(69,132,180) 28%,rgb(31,71,120) 64%);
			background-image: -moz-linear-gradient(bottom,rgb(69,132,180) 28%,rgb(31,71,120) 64%);
			background-image: -webkit-linear-gradient(bottom,rgb(69,132,180) 28%,rgb(31,71,120) 64%);
			background-image: -ms-linear-gradient(bottom,rgb(69,132,180) 28%,rgb(31,71,120) 64%);
			background-image: -webkit-gradient(linear,left bottom,left top,color-stop(0.28,rgb(69,132,180)),color-stop(0.64,rgb(31,71,120)));
      	}
      
      	.l-content{
      		position:relative;      		
      		min-height:500px;     		
      	}
      	
      	
      	.l-footer{
      		margin-top:0;
      	}	
      
      	#viewport{
      		position:absolute;
      		top:0;
      		left:0;
      		right:0;
      		bottom:0;
      		overflow:hidden;
      		perspective:400;
      	}
      	
      	#world{
      		position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -256px;
			margin-top: -256px;
			height: 512px;
			width: 512px;
			-webkit-transform-style: preserve-3d;
			-moz-transform-style: preserve-3d;
			-o-transform-style: preserve-3d;
      	}
      	
      	.cloudBase{
      		position: absolute;
			left: 256px;
			top: 256px;
			width: 20px;
			height: 20px;
			margin-left: -10px;
			margin-top: -10px;    		
			-webkit-transform-style: preserve-3d;
			-moz-transform-style: preserve-3d;
			-o-transform-style: preserve-3d;
      	}
      	
      	.cloudLayer{
			position: absolute;
			left: 50%;
			top: 50%;
			width: 256px;
			height: 256px;
			margin-left: -128px;
			margin-top: -128px;
			-webkit-transition: opacity .5s ease-out;
			-moz-transition: opacity .5s ease-out;
			-o-transition: opacity .5s ease-out;
      	}
      	
      	#viewport img{
      		max-width:1000px;
      	}
      
      </style>
      
      <div id="viewport">
      	<div id="world"></div>      
      </div>
      
      <script>
		var world = document.getElementById("world"),
			viewport = document.getElementById("viewport"),
			worldXAngle = 0,
			worldYAngle = 0,
			d = 0,
			p = 400;

		viewport.style.webkitPerspective = p;
		viewport.style.MozPerspective = p;
		viewport.style.oPerspective = p;
			

		var objects = [],
			layers = [];

		window.addEventListener("mousemove",function( e ){
			worldYAngle = -( .5  - ( e.clientX/ window .innerWidth ) ) * 180;
			worldXAngle = -( .5  - ( e.clientY/ window .innerHeight ) ) * 180;
			updateView();			
		});

		function generate(){
			objects = [];
			layers = [];

			if(world.hasChildNodes()){
				while ( world.childNodes.length >= 1){
					world.removeChild( world.firstChild );
				}
			}

			for(var j = 0;j <= 3 ; j++){
				objects.push(createCloud());
			}
		}


		function createCloud(){

			var div = document.createElement('div');
			div.className = "cloudBase";

			var x = 256 - ( Math.random() * 512 );
			var y = 256 - ( Math.random() * 512 );
			var z = 256 - ( Math.random() * 512 );
			var t = 'translateX(' + x + 'px) \
					translateY(' + y + 'px) \
					translateZ(' + z + 'px)';

			div.style.transform = t;
			div.style.webkitTransform = t;
			div.style.MozTransform = t;
			div.style.oTransform = t;
			
			world.appendChild(div);

			for(j = 0 ; j < 5 + Math.round( Math.random() * 10 ); j++) {
				var cloud = document.createElement("img");
				cloud.style.opacity = 0;

				var r = Math.random();
				var src = "<?php print base_path().drupal_get_path("theme", "radiohub")?>/images/cloud.png";

				( function( img ) { img.addEventListener( 'load', function() {
					img.style.opacity = .8;
				} ) } )( cloud );

				cloud.setAttribute( 'src', src );	
				cloud.className = "cloudLayer";

				var x = 256 - ( Math.random() * 512 );
				var y = 256 - ( Math.random() * 512 );
				var z = 100 - ( Math.random() * 200 );
				var a = Math.random() * 360;
				var s = .25 + Math.random();
					
				x *= .2;
				y *= .2;
				
				cloud.data = {
					x: x, 
					y: y,
					z: z,
					a: a,
					s: s,
					speed: .1 * Math.random(),
				}

				var t = 'translateX( ' + cloud.data.x + 'px ) translateY( ' + cloud.data.y + 'px ) translateZ( ' + cloud.data.z + 'px ) rotateZ( ' + cloud.data.a + 'deg ) scale( ' + cloud.data.s + ' )';

				cloud.style.transform = t;
				cloud.style.webkitTransform = t;
				cloud.style.MozTransform = t;
				cloud.style.oTransform = t;

				div.appendChild(cloud);
				layers.push (cloud);				
			} 

			return div;			
		}

		function update(){

			for(var j = 0 ;j < layers.length ; j ++){
				var layer = layers[j];
				layer.data.a += layer.data.speed;

				var t = 'translateX( ' + layer.data.x + 'px ) translateY( ' + layer.data.y + 'px ) translateZ( ' + layer.data.z + 'px ) rotateY( ' + ( - worldYAngle ) + 'deg ) rotateX( ' + ( - worldXAngle ) + 'deg ) rotateZ( ' + layer.data.a + 'deg ) scale( ' + layer.data.s + ')';
				
				layer.style.transform = t;
				layer.style.webkitTransform = t;
				layer.style.MozTransform = t;
				layer.style.oTransform = t;
			}
			
			requestAnimationFrame(update);
		}

		function updateView(){
			var t = 'translateZ( ' + d + 'px ) rotateX( ' + worldXAngle + 'deg) rotateY( ' + worldYAngle + 'deg)';
			
			world.style.transform = t;
			world.style.webkitTransform = t;
			world.style.MozTransform = t;
			world.style.oTransform = t;  
		}

		generate();
		update();
		
      </script>
      
      <?php print $feed_icons; ?>
    </div>

    <?php print render($page['sidebar_first']); ?>
    <?php print render($page['sidebar_second']); ?>
  </div>  
</div>

<footer class="l-footer" role="contentinfo">
	
	<?php print render($page['footer_1']); ?>
	<?php print render($page['footer_2']); ?>
	<?php print render($page['footer_3']); ?>
	<?php print render($page['footer_4']); ?>
    <?php print render($page['footer']); ?>
</footer>
<div class="clearfix"></div>
<div id="copyright">© <?php print Date("Y")?> <strong><?php print variable_get("site_name","") ?></strong> <?php print t("All rights reserved")?></div>
