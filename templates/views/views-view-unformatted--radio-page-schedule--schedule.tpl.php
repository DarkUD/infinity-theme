<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$week = array(
	"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"
);
?>
<?php if (!empty($title)): ?>
<div class="day-schedule clearfix">
<div class="wrapper">
  <h3 class="day-schedule-title"><?php print t(":day 's programmation",array(':day'=>$title)); ?></h3>
  <table class="day-schedule-table">
  	<tr>
  	<?php foreach ($week as $day):?>
  	
  		<?php 
  			if(t($day) != $title){
				print "<td>" . substr($day, 0,1) . "</td>";
			}else{
				print '<td class="current">'. $day . "</td>";
			}
  		?>
  	
  	<?php endforeach;?>
  	</tr>
  </table>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
</div>
</div>