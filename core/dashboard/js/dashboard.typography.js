(function ($) {

  Drupal.behaviors.infinityThemeDashboardTypographyBehavior = {
    attach: function (context, settings) { 	
    	
    	console.log("Loading Typography");
		
    	$("#section-typography").once("gb",function(){
    		$(".new-font .infinity-button").click(function(){
    			var name = $(".new-font .font-name input").val();
    			add_new_font(name);
    		});
    		
    		// Then the name
			$(".font-name input").change(function(){
				var item = $(this).parents(".font-item");
				name = $(this).val();
				key_name = get_key_name(name);
				
				item.attr("data-name",name);
				item.attr("data-key-name",key_name);
				
				load_font(name);
				
				save_fonts();
			});
			
			// And the remove button
			$(".font-remove").click(function(){
				var item = $(this).parents(".font-item");
				item.velocity("transition.slideLeftOut",function(){item.remove();save_fonts();});
			});
			
			
			load_fonts_variables();
    	});
    	
    	// We add a new font
    	function add_new_font(name){
    		var clone = $(".font-item.default").clone();
    		
    		fontname = get_font_name(name);
			key_name = get_key_name(name);
			
    		clone.attr("data-name",fontname);
    		clone.attr("data-key-name",key_name);
    		clone.find(".font-name input").val(name);
    		
    		clone.removeClass("default");
			clone.addClass("new");
			
			$('.font-item[data-name = "'+fontname+'"]').velocity("transition.slideLeftOut",function(){$(this).remove();})
			
			$(".fonts-holder").prepend(clone);
			
			clone.velocity("transition.slideLeftIn",function(){clone.removeClass("new");})
			
			load_font(name);
			save_fonts();
			
			// Then the name
			clone.find(".font-name input").change(function(){
				var item = $(this).parents(".font-item");
				name = $(this).val();
				key_name = get_key_name(name);
				
				item.attr("data-name",name);
				item.attr("data-key-name",key_name)
				
				load_font(name);
				
				save_fonts();
			});
			
			// And the remove button
			clone.find(".font-remove").click(function(){
				var item = $(this).parents(".font-item");
				item.velocity("transition.slideLeftOut",function(){item.remove();save_fonts();});
			});
    	}
    	
    	// We save the fonts
    	function save_fonts(){
    		var fonts = {};
    		
    		$(".fonts-holder .font-item").each(function(){
    			if($(this).hasClass("default")) return;
    			
    			var key_name = $(this).attr("data-key-name");
    			var name = $(this).find(".font-name input").val();
    			
    			fonts[key_name] = name;
    		});
    		
    		data = JSON.stringify(fonts);
    		$('input[name="fonts"]').val(data);
    		
    		load_fonts_variables();
    	}
    	
    	// We load the fonts variables to be used in custom css
    	function load_fonts_variables(){
    		var data = $('input[name="fonts"]').val();
    		
    		console.log(data);
    		
    		if(data){
    			fonts = JSON.parse(data);
        		
        		fonts_html = "";
        		
        		for(key_name in fonts){
        			name = fonts[key_name];
        			fontname = get_font_name(name);
        			load_font(name);
        			
        			fonts_html += '<div>@' + key_name + ' : <span class="font-demo" style="font-family:\''+fontname+'\';">Grumpy wizards ...</span></div>';
        		}
        	
        		$(".fonts-variables").html(fonts_html);
    		}
    	}
    	
    	// Load a font for the demo
    	function load_font(name){
    		WebFont.load({
			    google: {
			      families: [name]
			    }
			});
			
    		fontname = get_font_name(name);
			key_name = get_key_name(name);
						
			$('[data-key-name="'+key_name+'"]').css("font-family",fontname);
    	}
    	
    	// Helpers    	
    	function get_font_name(name){
    		var fontname = name.split(":");
			fontname = fontname[0];
			return fontname;
    	}
    	
    	function get_key_name(name){
    		fontname = get_font_name(name);
    		key_name = fontname.replace(/ /g,"_");
    		key_name = key_name.replace(/\+/g,"_");
    		key_name = key_name.toLowerCase();
    		return key_name;
    	}
    		
    }
  };

})(jQuery);
