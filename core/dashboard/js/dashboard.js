(function ($) {
	
  Drupal.behaviors.infinityThemeDashboardBehavior = {
    attach: function (context, settings) {
    	
    	// Define buttons behaviors
    	$("#sidebar-links li").unbind("click");
    	$("#sidebar-links li").click(function(){
    		var target = $(this).attr("data-target");
    		$(".section.active").removeClass("active");
    		$("#section-"+target).addClass("active");
    		
    		$("#sidebar-links li").removeClass("active");
    		$(this).addClass("active");
    		
    		save_state();
    	});
    	
    	// Now we load the state of the dashboard but only at the loading of the page then just once
    	$("#sidebar-links").once("gb",function(){
    		load_state();
    	});    	
    	
    	// For the morph-widget
    	$(".morph-trigger").click(function(){
    		var widget = $(this).parents(".morph-widget");
    		var content = widget.find(".morph-content");
    		content.velocity("transition.slideDownIn");
    		$(this).velocity("transition.fadeOut");
    	});
    	
    	$(".morph-content .cancel").click(function(){
    		var widget = $(this).parents(".morph-widget");
    		var content = widget.find(".morph-content");
    		var trigger = widget.find(".morph-trigger");
    		content.velocity("transition.slideUpOut");
    		trigger.velocity("transition.fadeIn");
    	});
    	
    	// Tabs loading
        $(".tabbedcontent").tabbedContent();
        
    	// We initialize the editors
    	
    	// Custom colors editor
    	var default_value = $('input[name="custom_colors"]').val();
    	$("#custom-colors-editor").html(default_value);
    	
    	var custom_colors_editor = ace.edit("custom-colors-editor");
    	custom_colors_editor.setTheme("ace/theme/twilight");
    	custom_colors_editor.getSession().setMode("ace/mode/less");
    	custom_colors_editor.getSession().on('change', function () {
    		$('input[name="custom_colors"]').val(custom_colors_editor.getSession().getValue());
    	});
    	
    	// Custom typo editor
    	var default_value = $('input[name="custom_typography"]').val();
    	$("#custom-fonts-editor").html(default_value);
    	
    	var custom_fonts_editor = ace.edit("custom-fonts-editor");
    	custom_fonts_editor.setTheme("ace/theme/twilight");
    	custom_fonts_editor.getSession().setMode("ace/mode/less");
    	custom_fonts_editor.getSession().on('change', function () {
    		$('input[name="custom_typography"]').val(custom_fonts_editor.getSession().getValue());
    	});
    	
    	// Custom css/js editors
    	
    	var default_value = $('input[name="custom_css"]').val();
    	$("#css-editor").html(default_value);
    	
    	var csseditor = ace.edit("css-editor");
        csseditor.setTheme("ace/theme/twilight");
        csseditor.getSession().setMode("ace/mode/less");
        csseditor.getSession().on('change', function () {
    		$('input[name="custom_css"]').val(csseditor.getSession().getValue());
    	});
        
        var default_value = $('input[name="custom_js"]').val();
    	$("#js-editor").html(default_value);
    	
        var jseditor = ace.edit("js-editor");
        jseditor.setTheme("ace/theme/twilight");
        jseditor.getSession().setMode("ace/mode/javascript");
        jseditor.getSession().on('change', function () {
    		$('input[name="custom_js"]').val(jseditor.getSession().getValue());
    	});
        
    	// Save the current state of the dashboard
    	function save_state(){
    		// We get the current section
    		var current_section = $("#sidebar-links li.active").attr("data-target");
    		var state = {
    			"section":current_section
    		};
    		
    		state = JSON.stringify(state);
    		
    		Cookies.set("inf-dashboard",state);
    	}
    	
    	// We load the previously saved state
    	function load_state(){
    		// We get the cookie ...
        	var state = Cookies.get("inf-dashboard");
        	
        	// and we restore the state
        	state = JSON.parse(state);
        	
        	var section = state['section'];
        	console.log(section);
        	$('#sidebar-links li[data-target="'+section+'"').click();
    	}
    }
  };

})(jQuery);
