(function ($) {

  Drupal.behaviors.infinityThemeDashboardProfilesBehavior = {
    attach: function (context, settings) { 	
    	
    	$("#section-profiles").once("gb",function(){
    		$(".new-profile .infinity-button").click(function(){
    			var name = $(".new-profile .profile-name input").val();
    			if(name == "") name = "default";
    			$('input[name="save_profile"]').val(name);
    		});
    		
			// And the remove button
			$(".profile-remove").click(function(){
				var item = $(this).parents(".item");
				var name = item.attr("data-profile-name");
				$('input[name="delete_profile"]').val(name);
			});
			
			$(".profile-load").click(function(){
				var item = $(this).parents(".item");
				var name = item.attr("data-profile-name");
				$('input[name="load_profile"]').val(name);
			});
    	});    		
    }
  };

})(jQuery);
