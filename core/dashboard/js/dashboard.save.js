(function ($) {

  Drupal.behaviors.infinityThemeDashboardSaveBehavior = {
    attach: function (context, settings) { 	
    	
    	$(".item-checkbox").unbind("change");
    	$(".item-checkbox").change(function(){
    		var target = $(this).attr("data-target-section");
    		var key = $(this).attr("data-key");
    		var data = $('input[name="'+target+'"]').val();
    		
    		console.log(target);
    		console.log(data);
    		
    		if(!data){
    			data = {};
    		}else{
    			data = JSON.parse(data);
    		}
    		
    		if($(this).attr("checked")){
    			data[key] = true;
    		}else{
    			delete data[key];
    		}
    		
    		data = JSON.stringify(data);
    		console.log(data);
    		
    		$('input[name="'+target+'"]').val(data);
    	});
    	
    }
  };

})(jQuery);
