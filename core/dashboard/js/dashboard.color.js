(function ($) {

  Drupal.behaviors.infinityThemeDashboardColorBehavior = {
    attach: function (context, settings) {
    	$("#section-colors").once("gb",function(){
    		$(".colorbox").ColorPicker({
    			onShow: function (colpkr) {
    				$(colpkr).fadeIn(500);
    				return false;
    			},
    			onHide: function (colpkr) {
    				$(colpkr).fadeOut(500);
    				var hex = $(".colorbox.active").attr("data-hex");
    				
    				var item = $(".colorbox.active").parents(".color-item");
    				var name = item.find(".color-name input");
    				
    				if(name.val() == ""){
    					name.val("color_" + hex);    					
    				}
    				
    				item.attr("data-hex",hex);
        			item.attr("data-name",name.val());
    				
    				$(".colorbox.active").removeClass("active");
    				
    				if( !$(this).hasClass("new") ) save_colors();
    				return false;
    			},
    			onBeforeShow: function () {
    				$(this).ColorPickerSetColor(this.value);
    				$(this).addClass("active");
    			},
    			onChange: function (hsb, hex, rgb) {
    				$(".colorbox.active").css('backgroundColor', '#' + hex);
    				$(".colorbox.active").css('color', '#' + hex);
    				$(".colorbox.active").val("#" + hex);
    				$(".colorbox.active").attr("data-hex",hex);
    			},
    			onSubmit: function(hsb, hex, rgb, el) {
    				console.log(el);
    				$(el).val("#"+ hex);   				
    			},
    		}); 
    		
    		// When the name is changed
    		$(".color-name input").change(function(){
				var item = $(this).parents(".color-item");
				
				if($(this).val() == ""){
					var hex = item.attr("data-hex");
					$(this).val("color_"+hex);
				}
				
				item.attr("data-name",$(this).val());
				save_colors();
			});
			
			// When the remove button is clicked
    		$(".color-remove").click(function(){
				var item = $(this).parents(".color-item");
				item.velocity("transition.slideLeftOut",function(){item.remove();});
				save_colors();
			});
    		
    		// To add a new color
    		$(".new-color .infinity-button").click(function(){
    			var hex = $(".new-color .colorbox").attr("data-hex");
    			var name = 'color_'+hex;
    			
    			add_new_color(hex,name);
    			save_colors();
    		});
    		
    		load_colors_variables();
    	});
    	
    	
    	// To add a new color
    	function add_new_color(hex,name){
    		// We clone the default color item
			var clone = $(".color-item.default").clone();
						
			clone.attr("data-hex",hex);
			clone.attr("data-name",name);
			
			clone.find(".colorbox").attr("data-hex",hex);
			clone.find(".colorbox").css('backgroundColor', '#' + hex);
			clone.find(".colorbox").css('color', '#' + hex);
			clone.find(".colorbox").val("#" + hex);
			
			clone.find(".color-name input").val(name);    			
			clone.removeClass("default");
			clone.addClass("new");
				
			$('.color-item[data-name = "color_'+hex+'"][data-hex = "'+hex+'"]').velocity("transition.slideLeftOut",function(){$(this).remove();})
			
			$(".colors-holder").prepend(clone);
			
			clone.velocity("transition.slideLeftIn",function(){clone.removeClass("new");})
			
			// We register the colorbox			
			clone.find(".colorbox").ColorPicker({
    			onShow: function (colpkr) {
    				$(colpkr).fadeIn(500);
    				return false;
    			},
    			onHide: function (colpkr) {
    				$(colpkr).fadeOut(500);
    				var hex = $(".colorbox.active").attr("data-hex");
    				
    				var item = $(".colorbox.active").parents(".color-item");
    				var name = item.find(".color-name input");
    				
    				if(name.val() == ""){
    					name.val("color_" + hex);    					
    				}
    				
    				item.attr("data-hex",hex);
        			item.attr("data-name",name.val());
    				
    				$(".colorbox.active").removeClass("active");
    				
    				if( !$(this).hasClass("new") ) save_colors();
    				return false;
    			},
    			onBeforeShow: function () {
    				$(this).ColorPickerSetColor(this.value);
    				$(this).addClass("active");
    			},
    			onChange: function (hsb, hex, rgb) {
    				$(".colorbox.active").css('backgroundColor', '#' + hex);
    				$(".colorbox.active").css('color', '#' + hex);
    				$(".colorbox.active").val("#" + hex);
    				$(".colorbox.active").attr("data-hex",hex);
    			},
    			onSubmit: function(hsb, hex, rgb, el) {
    				console.log(el);
    				$(el).val("#"+ hex);   				
    			},
			});
			
			// Then the name
			clone.find(".color-name input").change(function(){
				var item = $(this).parents(".color-item");
				
				if($(this).val() == ""){
					var hex = item.attr("data-hex");
					$(this).val("color_"+hex);
				}
				
				item.attr("data-name",$(this).val());
				save_colors();
			});
			
			// And finally the remove button
			clone.find(".color-remove").click(function(){
				var item = $(this).parents(".color-item");
				item.velocity("transition.slideLeftOut",function(){item.remove();save_colors();});				
			});
    	}
    	
    	// We save the colors
    	function save_colors(){
    		var colors = {};
    		
    		$(".colors-holder .color-item").each(function(){
    			if($(this).hasClass("default")) return;
    			
    			var hex = $(this).attr("data-hex");
    			var name = $(this).attr("data-name");
    			
    			colors[hex] = name;
    		});
    		
    		data = JSON.stringify(colors);
      		
    		$('input[name="colors"]').val(data);
    		
    		load_colors_variables();
    	}
    	
    	// Load Color variables in the .colors-variables holders
    	function load_colors_variables(){
    		var data = $('input[name="colors"]').val();
    		
    		if(data){
    			colors = JSON.parse(data);
        		
        		colors_html = "";
        		
        		for(hex in colors){
        			colors_html += '<div> <span class="color-demo" style="background:#'+hex+';"></span> @' + colors[hex] + '</div>';
        		}
        	
        		$(".colors-variables").html(colors_html);
    		}
    		
    	}
    }
  };

})(jQuery);
