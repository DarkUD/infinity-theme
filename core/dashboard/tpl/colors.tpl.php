<div id="section-<?php print $section; ?>" class="section<?php print $classes; ?>">
	<h2 class="section-title"><?php print infinity_icon($icon)?> <?php print t($title)?></h2>
	<div class="description">
		<?php print t("Manage the colors that will be used on the website.")?>
	</div>
	<ul id="colors-tabs" class="tabbedcontent-tabs">
		<li><a href="#colors-swatch"><?php print infinity_icon("tint")?> <?php print t("Colors swatch")?></a></li>
		<li><a href="#colors-custom"><?php print infinity_icon("code")?> <?php print t("Custom css")?></a></li>
	</ul>
	<div class="colors-tabcontent tabbedcontent">
		<div id="colors-swatch">
			<div class="actions">
				<div class="new-color">
					<input class="colorbox new" value="#000000" data-hex="ffffff"> <div class="infinity-button"><?php print infinity_icon("plus")?> <?php print t("Add color")?></div>
				</div>				
			</div>
			<div class="colors-holder">
				<div class="color-item default" data-hex="ffffff" data-name="white">
					<input class="colorbox" value="#ffffff">
					<div class="color-name"><input class="infinity-textfield" placeholder="<?php print t("Color name")?>"></div>
					<div class="color-remove infinity-button"><?php print infinity_icon("close")?></div>
				</div>
				
				<?php $colors = isset($settings["colors"]) ? $settings["colors"] : array();?>
				<?php foreach($colors as $hex => $color):?>
					<div class="color-item" data-hex="<?php print $hex?>" data-name="<?php print $color?>">
						<input class="colorbox" value="<?php print "#".$hex?>" style="background:<?php print "#".$hex?>; color:<?php print "#".$hex?>;">
						<div class="color-name"><input class="infinity-textfield" placeholder="<?php print t("Color name")?>" value="<?php print $color?>"></div>
						<div class="color-remove infinity-button"><?php print infinity_icon("close")?></div>
					</div>	
				<?php endforeach;?>
			</div>
		
		</div>
		<div id="colors-custom">
			<div class="variables-description">
				<?php print t("You can use the following variables in your custom css :")?>
				<div class="colors-variables"></div>
			</div>
			<div id="colors-css-editor-wrapper">
				<div id="custom-colors-editor" class="ace-editor css"></div>
			</div>
		</div>
	</div>
	

</div>
