<div id="section-<?php print $section; ?>" class="section<?php print $classes; ?>">
	<h2 class="section-title"><?php print infinity_icon($icon)?> <?php print t($title)?></h2>
	<div class="description">
		<?php print t("All the icons you can use")?>
		
		<?php 
			$icons_default = isset($settings["icons"]) ? $settings["icons"] : array();
		?>
	</div>
	<div class="section-content">
		<div class="choice">
			<label class="control-label" for="icons-enabled">Use Icon pack</label>
			<div class="control">
				<div class="slide-checkbox">	
					<input class="item-checkbox" type="checkbox" data-target-section="icons" data-key="enabled" value="none" id="icons_enabled" name="icons_enabled" <?php if(isset($icons_default->enabled)) print "checked";?>/>
					<label for="icons_enabled"></label>
				</div>
			</div>
		</div>
		<?php 
			$base_url 	= $GLOBALS["base_url"];
			$theme 		= $GLOBALS['theme_key'];
			$path 		= drupal_get_path('theme', $theme);
		?>
		<iframe width="100%" height="500px" src="<?php print $base_url . "/". $path. "/core/fonts/gb-font/demo.html" ?>"></iframe>
	</div>
</div>
