<div id="section-<?php print $section; ?>" class="section<?php print $classes; ?>">
	<h2 class="section-title"><?php print infinity_icon($icon)?> <?php print t($title)?></h2>
	<div class="section-content">
	<div class="description">
		<?php print t("Please select the Styles that should be added to the theme.")?>
	</div>
		<?php 
			$grids_default = isset($settings["grids"]) ? $settings["grids"] : array();
			
			$categories = $components["frameworks"];
			ksort($categories);
			?>
		<?php foreach($categories as $category => $items):?>
		
		<h3 class="items-category-title"><?php print t($category)?></h3>
		<div class="items-holder">		
		<?php $zebra = true;?>
		<?php foreach($items as $key => $item):?>
		
			<div class="item <?php print $zebra ? "odd" : "even"; $zebra = !$zebra;?>">
				<div class="teaser">
					<div class="item-name">
						<label class="control-label item-label" for="frameworks_<?php print $key?>"><?php print $item->name?></label>
						<div class="item-description"><?php print isset($item->description) ? $item->description : "";?></div>
						<div class="item-link"><?php if(isset($item->link)) print '<a href="'.$item->link.'" target="_blank">'.infinity_icon("external-link")." ".$item->name . " " . t("link") . "</a>";?></div>
					</div>
					<div class="item-operations">
						<div class="control">
							<div class="slide-checkbox">	
								<input type="checkbox" value="none" data-target-section="grids" data-key="<?php print $key?>" class="item-checkbox" id="frameworks_<?php print $key?>" name="frameworks_<?php print $key?>" <?php if(isset($grids_default->$key)) print "checked";?> />
								<label for="frameworks_<?php print $key?>"></label>
							</div>
						</div>				
					</div>
				</div>			
			</div>
			
		<?php endforeach;?>
		</div>	
	<?php endforeach;?>	
	
	</div>
</div>
