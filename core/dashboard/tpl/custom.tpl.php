<div id="section-<?php print $section; ?>" class="section<?php print $classes; ?>">
	<h2 class="section-title"><?php print infinity_icon($icon)?> <?php print t($title)?></h2>
	<div class="description"><?php print("You can edit here the custom css and js that will be added to the website")?></div>
	<div class="section-content">
		<ul id="custom-tabs" class="tabbedcontent-tabs">
			<li><a href="#css-editor-wrapper"><?php print t("Custom CSS")?></a></li>
			<li><a href="#js-editor-wrapper"><?php print t("Custom JS")?></a></li>
		</ul>
		<div class="tabbedcontent">
			<div id="css-editor-wrapper">
				<div class="variables-description">
					<?php print t("You can use the following variables in your custom css :")?>
					<div class="colors-variables"></div>
					<div class="fonts-variables"></div>
				</div>	
				<div id="css-editor" class="ace-editor">.css{}</div>
			</div>
			
			<div id="js-editor-wrapper">		
				<div class="variables-description">
					<?php print t('You might want to check this link for some insight with javascript management : <a href="https://www.drupal.org/node/172169">Drupal Javascript best practices</a>')?>
				</div>
				<div id="js-editor" class="ace-editor">function something(){}</div>
			</div>
		</div>
	</div>
</div>
