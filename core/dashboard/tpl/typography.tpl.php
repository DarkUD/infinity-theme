<div id="section-<?php print $section; ?>" class="section<?php print $classes; ?>">
	<h2 class="section-title"><?php print infinity_icon($icon)?> <?php print t($title)?></h2>
	<div class="description">
		<?php print t("Manage the fonts that will be used on the website.")?>
	</div>
	<ul id="fonts-tabs" class="tabbedcontent-tabs">
		<li><a href="#fonts-library"><?php print infinity_icon("font")?> <?php print t("Font library")?></a></li>
		<li><a href="#fonts-custom"><?php print infinity_icon("code")?> <?php print t("Custom css")?></a></li>
	</ul>
	<div class="fonts-tabcontent tabbedcontent">
		<div id="fonts-library">
			<div class="actions">
				<div class="new-font">
					<div class="font-name"><input class="infinity-textfield" placeholder="<?php print t("Google Font name")?>"></div> <div class="infinity-button"><?php print infinity_icon("plus")?> <?php print t("Add font")?></div>
				</div>				
			</div>
			<div class="fonts-holder">
				<div class="font-item default" data-name="arial">
					<div class="font-name"><input class="infinity-textfield" placeholder="<?php print t("Google Font name")?>"></div>
					<div class="font-remove infinity-button"><?php print infinity_icon("close")?></div>
					<div class="font-demo">Grumpy wizards make toxic brew for the evil Queen and Jack.</div>
				</div>
				<?php $fonts = isset($settings["typography"]) ? $settings["typography"] : array();?>
				<?php foreach($fonts as $key_name => $font):?>
					<div class="font-item" data-key-name="<?php print $key_name?>" data-name="<?php print $font?>">
						<div class="font-name"><input class="infinity-textfield" placeholder="<?php print t("Google Font name")?>" value="<?php print $font?>"></div>
						<div class="font-remove infinity-button"><?php print infinity_icon("close")?></div>
						<div class="font-demo">Grumpy wizards make toxic brew for the evil Queen and Jack.</div>
					</div>	
				<?php endforeach;?>
			</div>
		</div>
		<div id="fonts-custom">
			<div class="variables-description">
				<?php print t("You can use the following variables in your custom css :")?>
				<div class="fonts-variables"></div>
			</div>
			<div id="fonts-css-editor-wrapper">
				<div id="custom-fonts-editor" class="ace-editor css"></div>
			</div>
		</div>
	</div>
	
</div>
