<div id="section-<?php print $section; ?>" class="section<?php print $classes; ?>">
	<h2 class="section-title"><?php print infinity_icon($icon)?> <?php print t($title)?></h2>
	<div class="description"><?php print("You can manage the profiles saved on the website")?></div>
	<div class="actions">
		<div class="new-profile">
			<div class="profile-name"><input class="infinity-textfield" placeholder="<?php print t("Profile name")?>"></div> <button class="infinity-button"><?php print infinity_icon("plus")?> <?php print t("Save Profile")?></button>
		</div>				
	</div>
	<div class="profiles-holder">
		<?php 
			$categories = isset($profiles["profiles"]) ? $profiles["profiles"] : array();
			ksort($categories);
		?>
		
		<?php foreach($categories as $category => $items):?>
		
		<h3 class="items-category-title"><?php print t($category)?></h3>
		<div class="items-holder">		
		<?php $zebra = true;?>
		<?php foreach($items as $key => $item):?>
			<div class="item <?php print $zebra ? "odd" : "even"; $zebra = !$zebra;?>" data-profile-name="<?php print $item->name;?>">
				<div class="teaser">
					<div class="item-name">
						<label class="control-label item-label" for="frameworks_<?php print $key?>"><?php print $item->name?></label>
						<div class="item-description"><?php print isset($item->description) ? $item->description : "";?></div>
						<div class="item-link"><?php if(isset($item->link)) print '<a href="'.$item->link.'" target="_blank">'.infinity_icon("external-link")." ".$item->name . " " . t("link") . "</a>";?></div>
					</div>
					<div class="item-operations">
						<button class="profile-remove infinity-button"><?php print infinity_icon("close")?> <?php print t("Remove")?></button>
						<button class="profile-load infinity-button"><?php print infinity_icon("upload")?> <?php print t("Load")?></button>
					</div>
				</div>			
			</div>
			
		<?php endforeach;?>
		</div>	
		<?php endforeach;?>
	
		
	</div>
	
</div>
